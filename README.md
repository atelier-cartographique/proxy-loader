# proxy-loader

Proxyloader provides a simple backend to work with distant resources (geojson) on Cartostation.

Functionnalities are :

- Connection to remote ressources
- Projection transformations
- Data processing on the fly via python input

## Server

Examples :

- `data.mobility.brussels`
- `geodata.environnement.brussels`
- `maplog.atelier-cartographique.be`

## Layers

Examples:

**server** : `data.mobility.brussels`  
**path** : `geoserver/bm_bike/wfs?service=wfs&version=1.1.0&request=GetFeature&typeName=bm_bike:bike_paves_ss&outputFormat=json&srsName=EPSG:31370`

**server** : `geodata.environnement.brussels`  
**path** : `api/geodata/postgis/web_service/wsl_bruenvi_water_surface/`

**server** : `maplog.atelier-cartographique.be`  
**path** : `maps/cafe-des-savoirs-ganshoren.geojson`

### Transform proj :

**Source** : optional, should be declared following this structure: `EPSG:4326`

### Transform python :

Optional, could be used to process data on the fly.

## Metadata for proxy layer

the ressource identifier has to be declared as such :

`proxy://hostname/proxy_layer-ID`

Examples :

- `proxy://data.mobility.brussels/1`
- `proxy://geodata.environnement.brussels/2`
- `proxy://maplog.atelier-cartographique.be/9`
